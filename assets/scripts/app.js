let objs = {
    eclipse: {
        elem: false,
        notop: false,
        full: false,
    },
    nav: {
        elem: false,
    },
    right: {
        elem: false,
        openelem: false,
        closeelem: false,
        opened: false,
        transformelem: false,
        authbtn: false,
        regbtn: false,
        authactive: true,
    },
    items: {
        input: false,
        minus: false,
        plus: false,
        compare: false,
        like: false,
        max: 100,
    }
}
document.addEventListener("DOMContentLoaded", function(event) {
    initMainSlider();
    init();
    initItems();
});

function initMainSlider() {
    $('#main-slick').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        // arrows: false,
        prevArrow: "#main-prev",
        nextArrow: "#main-next",
        dots: false,
        infinite: true,
        pauseOnHover: true,
        speed: 300,
        // slidesToShow: 1,
        // slidesToScroll: 1,
        asNavFor: '#main-sub-slick'
    });
    $('#main-sub-slick').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '0px',
        asNavFor: '.slider-for',
        // focusOnSelect: true,
        dots: false,
        arrows: false,
        speed: 300,
        asNavFor: '#main-slick',
      });
}

function init() {
    objs.eclipse.elem = document.getElementById("eclipse");
    objs.nav.elem = document.getElementById("nav-left");

    objs.nav.elem.onmouseout = function() {
        if (objs.eclipse.notop) {
            objs.eclipse.elem.classList.remove("notop");
            objs.eclipse.notop = false;
        };
    };
    objs.nav.elem.onmouseover = function() {
        if (!objs.eclipse.notop) {
            objs.eclipse.elem.classList.add("notop");
            objs.eclipse.notop = true;
        };
    };

    objs.right.elem = document.getElementById("auth-panel");
    objs.right.openelem = document.getElementById("btn-person");
    objs.right.closeelem = document.getElementById("close-auth-panel");

    objs.right.openelem.onclick = function() {
        if (!objs.right.opened && !objs.eclipse.full) {
            objs.right.elem.classList.add("active");
            objs.eclipse.elem.classList.add("full");
            objs.right.opened = true;
            objs.eclipse.full = true;
        };
    };
    objs.right.closeelem.onclick = function() {
        if (objs.right.opened && objs.eclipse.full) {
            objs.right.elem.classList.remove("active");
            objs.eclipse.elem.classList.remove("full");
            objs.right.opened = false;
            objs.eclipse.full = false;
        };
    };   
    
    objs.eclipse.elem.onclick = function() {
        if (objs.eclipse.full) {
            objs.right.elem.classList.remove("active");
            objs.eclipse.elem.classList.remove("full");
            objs.right.opened = false;
            objs.eclipse.full = false;            
        }
    }

    objs.right.authbtn = document.getElementById("auth-btn");
    objs.right.regbtn = document.getElementById("reg-btn");
    objs.right.transformelem = document.getElementById("auth-transform");

    objs.right.authbtn.onclick = function() {
        if (!objs.right.authactive) {
            objs.right.transformelem.classList.remove("active");
            objs.right.authbtn.classList.add("active");
            objs.right.regbtn.classList.remove("active");
            objs.right.authactive = true;
        }
    };
    objs.right.regbtn.onclick = function() {
        if (objs.right.authactive) {
            objs.right.transformelem.classList.add("active");
            objs.right.authbtn.classList.remove("active");
            objs.right.regbtn.classList.add("active");
            objs.right.authactive = false;
        }
    };
}

function initItems() {
    objs.items.input = document.getElementsByClassName("item-count");
    objs.items.minus = document.getElementsByClassName("item-dec");
    objs.items.plus = document.getElementsByClassName("item-inc");
    objs.items.compare = document.getElementsByClassName("item-compare");
    objs.items.like = document.getElementsByClassName("item-like");

    for (let i = 0; i < objs.items.input.length; i++) {
        objs.items.minus[i].onclick = function() {
            let input = this.parentNode.getElementsByClassName("item-count")[0];
            if (+input.value > 1) {
                input.value = +input.value - 1;
            }
        }
        objs.items.plus[i].onclick = function() {
            let input = this.parentNode.getElementsByClassName("item-count")[0];
            if (+input.value < objs.items.max) {
                input.value = +input.value + 1;
            }
        }
        objs.items.input[i].onkeyup = function() {
            this.value = this.value.replace(/[^\d]/g,'');
            if (+this.value < 1) {
                this.value = 1;
            }
            if (+this.value > objs.items.max) {
                this.value = objs.items.max;
            }
        }
        objs.items.compare[i].onclick = function() {
            this.classList.toggle("active");
        }
        objs.items.like[i].onclick = function() {
            this.classList.toggle("active");
        }
    }
}